/**
 * @file Drupal Tabledrag drag & drop functionality.
 */
(function ($) {
	Drupal.behaviors.viewsCacheUI = {
		attach: function (context, settings) {
			$('table#views-cache-ui-table', context).once('field-display-overview', function () {
				Drupal.viewsCacheUI.attach(this);
			});
		}
	};

	Drupal.viewsCacheUI = {
		attach: function(table){
			var tableDrag = Drupal.tableDrag[table.id];
			tableDrag.onDrop = this.onDrop;
		},
		onDrop: function () {
			var dragObject = this,
				$table = $(dragObject.table),
				$ignored = $table.find("td.region-ignored-title").parent(),
				row = dragObject.rowObject.element,
				$row = $(row);
			$table.find("tbody > tr").each(function (index) { $(this).attr("data-weight", index); });
			var weight_ignored = parseInt($ignored.attr("data-weight")),
				weight_row = parseInt($row.attr("data-weight")),
				$ignore = $row.find("input.views-cache-ui-ignore");
			//If this row is after the ignore row
			if (weight_row > weight_ignored) {
				//check the ignore checkbox
				$ignore[0].checked = true;
			} else {
				$ignore[0].checked = false;
			}
		}
	};
})(jQuery);
