 Views Cache UI
 ==============

Provides a simple interface to set time-based caching rules on views and displays via a page at /admin/structure/views/settings/cache.

This module is limited to time-based caches to identify uncached views, other caches are not supported.

on [drupal.org](https://www.drupal.org/sandbox/malcolm_p/2483491)
